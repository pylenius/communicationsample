﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace CommunicationApi
{
    public static class HttpWebRequestExtensions
    {
        public static Task<TResponse> GetResponseAsync<TResponse>(this HttpWebRequest web)
        {
            var tcs = new TaskCompletionSource<TResponse>();
            web.BeginGetResponse(iar =>
            {
                var request = (HttpWebRequest)iar.AsyncState;

                try
                {
                    var response = request.EndGetResponse(iar);
                    TResponse fromApi;


                    if (response.ContentType.Contains("text/xml"))
                    {
                        var s = new XmlSerializer(typeof(TResponse));
                        fromApi = (TResponse)s.Deserialize(response.GetResponseStream());
                    }
                    else if (response.ContentType.Contains("application/json"))
                    {
                        using (var streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            var r = streamReader.ReadToEnd();
                            fromApi = JsonConvert.DeserializeObject<TResponse>(r);
                        }
                    }
                    else
                    {
                        throw new Exception("Unknown ContentType:" + response.ContentType);
                    }

                    tcs.TrySetResult(fromApi);
                }
                catch (OperationCanceledException)
                {
                    tcs.TrySetCanceled();
                }
                catch (Exception exc)
                {
                    tcs.TrySetException(exc);
                }
            }, web);
            return tcs.Task;
        }
    }

    public class Address_Components
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast2 northeast { get; set; }
        public Southwest2 southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public Address_Components[] address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string[] types { get; set; }
    }

    public class GoogleResponse
    {
        public Result[] results { get; set; }
        public string status { get; set; }
    }

}
