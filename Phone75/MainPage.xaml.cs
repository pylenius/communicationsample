﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CommunicationApi;
using Microsoft.Phone.Controls;

namespace Phone75
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            foo();
        }

        async void foo()
        {
            var foo = HttpWebRequest.CreateHttp("http://maps.googleapis.com/maps/api/geocode/json?address='Tampere'&sensor=false");
            foo.Accept = "application/json";
            var response = await foo.GetResponseAsync<GoogleResponse>();
            response.ToString();
        }
    }
}